#include <stdio.h>
#include <stdlib.h>

void fun1(int [10][10],int,int);
void fun2(int [10][10],int [10][10],int,int);
void fun3(int [10][10],int[10][10],int,int,int);
void fun4(int [10][10],int,int);

int main()
{   int x[10][10],y[10][10];
    int row1,row2,col1,col2;
    printf("For first matrix\n");
    printf("Enter no of rows please:");
    scanf("%d",&row1);
    printf("Enter no of columns please:");
    scanf("%d",&col1);
    printf("For second matrix:");
    printf("Enter no of columns please:");
    scanf("%d",&row2);
    printf("Enter no of columns please:");
    scanf("%d",&col2);
    printf("Enter elements of first matrix\n");
    fun1(x,row1,col1);
    printf("Enter elements of second matrix\n");
    fun1(y,row2,col2);
    if((row1==row2)&&(col1==col2))
        fun2(x,y,row1,col1);
    else
        printf("Multiplication not possible\n");
    if(col1==row2)
        fun3(x,y,row1,col1,col2);
    else
        printf("Multiplication not possible\n");
}

void fun1(int x[10][10],int row,int col)
{
    int m,n;
    for(m=0;m<row;m++)
    {
        for(n=0;n<col;n++)
        {
            scanf("%d",&x[m][n]);
        }
    }

}

void fun2(int x[10][10],int y[10][10],int row1,int col1)
{
    int m,n;
    int i[10][10];
    for(m=0;m<row1;m++){
        for(n=0;n<col1;n++){
            i[m][n]=x[m][n]+y[m][n];
        }
    }
    printf("Addition \n");
    fun4(i,row1,col1);
}

void fun3(int x[10][10],int y[10][10],int row1,int col1,int col2){
    int m,n,d[10][10],k;
    for(m=0;m<row1;m++){
        for(n=0;n<col2;n++){
            d[m][n]=0;
            for(k=0;k<col1;k++){
                d[m][n]=d[m][n]+(x[m][n]*y[n][m]);
            }
        }
    }
    printf("Multiplication:\n");
    fun4(d,row1,col1);
}

void fun4(int x[10][10],int row,int col){
    int m,n;
    for(m=0;m<row;m++){
        for(n=0;n<col;n++){
            printf("%d\n",x[m][n]);
        }
    printf("\n");
    }
}
