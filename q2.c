#include <stdio.h>
#include <stdlib.h>

int main(int gc, char *gv[])
{
    char x=gv[1][0];
    int y=0;
    for (int i=2; i<gc; i++){
            for(int j=0; j<strlen(gv[i]);j++)
                if(gv[i][j]==x) y++;
    }
    printf("FREQUENCY OF %c IN GIVEN SENTENCE IS : %d",x,y);
    return 0;
}
